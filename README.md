**Fork, clone, branch, and merge this repository**

---

## 1. Fork an existing repository

You�ll start by making a fork of this repository. Think of a fork as your own working copy of a repository. Any changes you make in your fork will not be reflected in the original repository.

1. In Bitbucket, navigate to the repository you wish to fork.
2. Click **+** in the left-most menu and select **Fork this repository**.
3. You'll now be asked to create a new repository for your fork. Select a Workspace, Project, and Name for your new repository. *Optional: Mark your reposity as Private if you wish to limit access to it*
4. Click **Fork repository**.
5. From your forked repository, click **Repository settings**.
6. Click **User and group access**. 
7. Enter the name or email address of anyone you wish to give access to your repository. *Please add your instructor*
8. Select the access level from the drop-down. *Please give your instructor **Write** permission* 

---

## 2. Clone your fork to your local machine

Cloning allows you to work on your files locally. Think of cloning as downloading a respository from the Bitbucket server. Any changes you make to the local files will not be reflected in your respository until they are explicitly committed and pushed back to the Bitbucket server. 
You can clone your repository from Sourcetree or Bitbucket:

**Clone from Sourcetree**

1. Open a new tab in Sourcetree and select the **Remote** heading.
2. Click **Add an account**.
3. Under **Credentials**, select **Basic** authentication from the drop-down and enter your Atlassian username.
4. Click **Refresh Password** and enter your password.
5. Select your fork from the list of remote repositories and click **Clone**.
4. Update the destination path to a new or *empty* folder on your local machine.
5. Click **Clone**.

**Clone from Bitbucket**

1. From the **Source** page of your forked repository, click the **Clone** button in the top-right corner.
2. Click **Clone in Sourcetree** in the modal. This will open a new tab in Sourcetree.
3. The URL to the repository should already be populated. Leave this as-is.
4. Update the destination path to a new or *empty* folder on your local machine.
5. Click **Clone**.

---

## 3. Branch your repository

Branching offers a way to work on a new feature without affecting the main codebase. You can branch your repository from Sourcetree or Bitbucket:

**Branch from Sourcetree**

1. Click the **Branch** heading.
2. Enter a name for your new branch.
3. Click **Create Branch**.

**Branch from Bitbucket**

1. From the **Branches** page of your forked repository, click the **Create branch** button in the top-right corner.
2. Enter a name for your new branch.
3. Click **Create**.

In Sourcetree, you can "checkout" branches by double-clicking on items in the **Branches** menu. Branches that have been checked out will appear **bolded**. 

---

## 4. Push changes to your branch

Any local changes you make to your files will appear under the **File Status** view in Sourcetree. When you are happy with your changes, you can commit and *push* them back to the Bitbucket server, where they are then available to be fetched and *pulled* by others.

1. From the **File Status** view, *stage* the files you wish to commit. You can choose to **Stage All**, which will commit all changes made to any files. Or can select individual files and choose **Stage Selected** to only commit a subset of changes.
2. Write a brief description of the changes you made in the blank text area at the bottom of the screen.
3. Review your staged files and description, then click **Commit**. You have now committed to your local changes but they have not yet been pushed to the Bitbucket server.
4. At the top of the screen, you should now see a badge on the **Push** button. Click **Push**.
5. Select the branch (or branches) you wish to push your changes to and click **Push**.

---

## 5. Pulling changes to your local machine

You can pull changes made to respositories to your local clone from Sourcetree.

1. Click **Fetch** at the top of the screen.
2. If there are any changes that you not yet pulled from the Bitbucket server, a badge will appear on the **Pull** button.
3. Click **Pull**.
4. Make sure you are pulling from the correct branch and click **OK**.

Your local files will now be updated to reflect any changes.

---

## 6. Merging your branches

Once you are satisfied with the work you complete on your branch you may wish to combine it with the main codebase. You can be *merge* branches from Sourcetree or Bitbucket:

**Merge from Sourcetree**

1. Checkout the branch you wish to merge *to* (this is usually your master branch) and click **Merge** at the top of the screen.
2. Any conflicts between the current branch and the branch you are merging with will be highlighted. These conflicts can be resolved either by discarding or combining changes to the same files.
3. Click **OK**.
4. Click **Push** at the top of the screen.

**Merge from Bitbucket**

1. From the **Branches** page of your forked repository, open the **Actions** overflow menu on the branch you wish to merge.
2. Select **Merge**.
3. Update the commit message if needed.
4. Click **Merge**.

At this point the branches will be merged. You may wish to checkout a new branch before you continue to work.

---